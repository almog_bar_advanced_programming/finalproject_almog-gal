#pragma once

#include "Includes.h"
#include "DataBase.h"
#include "Question.h"
#include "User.h"

class User;

class Game
{
private:
	std::vector<Question*> _question;
	std::vector<User*> _players;
	int _questionsNum;
	int _currQuestionIndex;
	DataBase& _db;
	std::map<std::string, int> _result;
	int _currentTurnAnswer;
	bool insertGameToDB();
	void initQuestionFromDB();
	void sendQuestionToAllUsers();
	int _id;
public:
 	Game(const std::vector<User*>& players, int questionNum, DataBase& db);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleAnswerFromUser(User* user, int a, int b);
	bool handleNextTurn();
	bool leaveGame(User* currUser);
	int getId();

};
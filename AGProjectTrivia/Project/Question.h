#pragma once

#include "Includes.h"

class Question
{
private:
	std::string _question;
	std::string _answer[4];
	int _currentAnswerIndex;
	int _id;

public:
	Question(int questionId, std::string question, std::string currentAnswer, std::string answer2, std::string answer3, std::string answer4);
	std::string getQuestion();
	std::string* getAnswer();
	int getCorrentAnswerIndex();
	int getId();
};
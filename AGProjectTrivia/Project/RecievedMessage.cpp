#include "RecievedMessage.h"

RecievedMessage::RecievedMessage(SOCKET sock, int messageCode)
{
	_sock = sock;
	_messageCode = messageCode;
}

RecievedMessage::RecievedMessage(SOCKET sock, int messageCode, vector<string> values) : RecievedMessage(sock, messageCode)
{
	_values = values;
}

SOCKET RecievedMessage::getSock()
{
	return _sock;
}


int RecievedMessage::getMessageCode()
{
	return _messageCode;
}


vector<string>& RecievedMessage::getValues()
{
	return _values;
}

User* RecievedMessage::getUser()
{
	return _user;
}

void RecievedMessage::setUser(User* u)
{
	this->_user = u;
}
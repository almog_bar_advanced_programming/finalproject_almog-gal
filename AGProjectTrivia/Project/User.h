#pragma once

#include "Includes.h"
#include "Room.h"
#include "Game.h"
#include "Helper.h"

class Room;
class Game;

class User
{
private:
	std::string _userName;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _socket;

public:
	User(std::string userName, SOCKET sock);
	void send(std::string msg);
	std::string getUserName();
	Room* getRoom();
	Game* getGame();
	void setGame(Game* game);
	void clearRoom();
	bool createRoom(int roomId, std::string roomName, int maxUsers, int questionNum, int questionTime);
	bool joinRoom(Room* newRoom);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	SOCKET getSocket();

};
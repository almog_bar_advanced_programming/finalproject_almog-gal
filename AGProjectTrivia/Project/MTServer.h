#pragma once

#include "Includes.h"
#include "RecievedMessage.h"
#include "Game.h"
#include "Validator.h"
#include "Question.h"
#include "Room.h"
#include "User.h"


// Q: why do we need this class ?
// A: this is the main class which holds all the resources,
// accept new clients and handle them.
class MTServer
{
public:
	MTServer();
	~MTServer();
	void serve();


private:

	void bindAndListen();
	void acceptClient();
	void clientHandler(SOCKET client_socket);
	void safeDeleteUser(RecievedMessage* msg);

	RecievedMessage* buildRecieveMessage(SOCKET client_socket, int msgCode);
	void handleRecievedMessages();
	void addRecievedMessage(RecievedMessage* msg);

	User* handleSignin(RecievedMessage* msg);
	bool handleSignup(RecievedMessage* msg);
	void handleSignout(RecievedMessage* msg);

	void handleLeaveGame(RecievedMessage* msg);
	void handleStartGame(RecievedMessage* msg);
	void handlePlayerAnswer(RecievedMessage* msg);

	bool handleCreateRoom(RecievedMessage* msg);
	bool handleCloseRoom(RecievedMessage* msg);
	bool handleJoinRoom(RecievedMessage* msg);
	bool handleLeaveRoom(RecievedMessage* msg);
	void handleGetUsersInRoom(RecievedMessage* msg);
	void handleGetRooms(RecievedMessage* msg);

	void handleGetBestScores(RecievedMessage* msg);
	void handleGetPersonalStatus(RecievedMessage* msg);

	User* getUserByName(string name);
	User* getUserBySocket(SOCKET sock);
	Room* getRoomById(int id);

	SOCKET _socket;

	std::map<SOCKET, User*> _connectedUsers;
	DataBase _db;
	std::map<int, Room*> _roomsList;

	std::queue<RecievedMessage*> _messageHandler;

	std::mutex _mtxRecievedMessages;
	std::condition_variable _msgQueueCondition;

};


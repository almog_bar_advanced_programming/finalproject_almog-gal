#include "Question.h"

/*
This function is the C'tor for Question
input:question id, question and 4 answers
output:question
*/
Question::Question(int questionId, std::string question, std::string currentAnswer, std::string answer2, std::string answer3, std::string answer4)
{
	std::string ans[4];
	ans[0] = currentAnswer;
	ans[1] = answer2;
	ans[2] = answer3;
	ans[3] = answer4;
	std::string a[3] = { answer2,answer3,answer4 };
	int index = 0;
	this->_id = questionId;
	srand(time(NULL));
	int rnd = rand() % 4;
	this->_answer[rnd] = currentAnswer;
	for (int i = 0; i < 4; i++)
	{
		if (i != rnd)
		{
			this->_answer[i] = a[index];
			index++;
		}
	}
	this->_currentAnswerIndex = 0;
	this->_question=question;

}

std::string Question::getQuestion()
{
	return this->_question;
}

std::string* Question::getAnswer()
{
	return this->_answer;
}

int Question::getCorrentAnswerIndex()
{
	return this->_currentAnswerIndex;
}

int Question::getId()
{
	return this->_id;
}
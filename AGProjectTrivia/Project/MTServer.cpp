#include "MTServer.h"
#include <exception>
#include <iostream>
#include <string>

// using static const instead of macros 
static const unsigned short PORT = 8820;
static const unsigned int IFACE = 0;
static int _roomIdSequence = 0;

MTServer::MTServer()
{
	// notice that we step out to the global namespace
	// for the resolution of the function socket
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	_db = *(new DataBase());
}

MTServer::~MTServer()
{
	TRACE(__FUNCTION__ " closing accepting socket");
	// why is this try necessarily ?
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_socket);
	}
	catch (...) {}
	std::map<SOCKET, User*>::iterator it;
	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); ++it)
		delete it->second;
	std::map<int, Room*>::iterator it2;
	for (it2 = _roomsList.begin(); it2 != _roomsList.end(); ++it2)
		delete it2->second;
}

void MTServer::serve()
{
	bindAndListen();

	// create new thread for handling message
	std::thread tr(&MTServer::handleRecievedMessages, this);
	tr.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		TRACE("accepting client...");
		acceptClient();
	}
}


// listen to connecting requests from clients
// accept them, and create thread for each client
void MTServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	TRACE("binded");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	TRACE("listening...");
}

void MTServer::acceptClient()
{
	SOCKET client_socket = accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	TRACE("Client accepted !");
	// create new thread for client	and detach from it
	std::thread tr(&MTServer::clientHandler, this, client_socket);
	tr.detach();

}



void MTServer::clientHandler(SOCKET client_socket)
{
	RecievedMessage* currRcvMsg = nullptr;
	try
	{
		// get the first message code
		int msgCode = Helper::getMessageTypeCode(client_socket);

		while (msgCode != 0 && msgCode != 299)
		{
			currRcvMsg = buildRecieveMessage(client_socket, msgCode);
			addRecievedMessage(currRcvMsg);

			msgCode = Helper::getMessageTypeCode(client_socket);
		}

		currRcvMsg = buildRecieveMessage(client_socket, 208);
		addRecievedMessage(currRcvMsg);

	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function clientHandler. socket=" << client_socket << ", what=" << e.what() << std::endl;
		currRcvMsg = buildRecieveMessage(client_socket, 208);
		addRecievedMessage(currRcvMsg);
	}
	closesocket(client_socket);
}

void MTServer::addRecievedMessage(RecievedMessage* msg)
{
	unique_lock<mutex> lck(_mtxRecievedMessages);

	_messageHandler.push(msg);
	lck.unlock();
	_msgQueueCondition.notify_all();

}

User* MTServer::getUserByName(string name)
{
	User* r = nullptr;
	for (map<SOCKET, User*>::iterator it = _connectedUsers.begin(); it != _connectedUsers.end(); ++it)
		if (it->second->getUserName() == name)
			r = it->second;
	return r;
}

User* MTServer::getUserBySocket(SOCKET sock)
{
	User* r = nullptr;
	if (_connectedUsers.find(sock) != _connectedUsers.end())
	{
		r = (_connectedUsers.find(sock))->second;
	}
	return r;
}

Room* MTServer::getRoomById(int id)
{
	Room* r = nullptr;
	if (_roomsList.find(id) == _roomsList.end())
	{
		r = (_roomsList.find(id))->second;
	}
	return r;
}

// remove the user from queue
void MTServer::safeDeleteUser(RecievedMessage* msg)
{
	try
	{
		SOCKET s = msg->getSock();
		handleSignout(msg);
		TRACE("REMOVED %s from users list", getUserBySocket(s)->getUserName());
		_connectedUsers.erase(s);
	}
	catch (...) {}

}

void MTServer::handleRecievedMessages()
{
	int msgCode = 0;
	SOCKET clientSock = 0;
	string userName;
	while (true)
	{
		try
		{
			unique_lock<mutex> lck(_mtxRecievedMessages);

			// Wait for clients to enter the queue.
			if (_messageHandler.empty())
				_msgQueueCondition.wait(lck);

			// in case the queue is empty.
			if (_messageHandler.empty())
				continue;

			RecievedMessage* currMessage = _messageHandler.front();
			_messageHandler.pop();
			lck.unlock();

			// Extract the data from the tuple.
			clientSock = currMessage->getSock();
			msgCode = currMessage->getMessageCode();

			try
			{
				switch (msgCode)
				{
				case 200:
					handleSignin(currMessage);
					break;
				case 201:
					handleSignout(currMessage);
					break;
				case 203:
					handleSignup(currMessage);
					break;
				case 205:
					handleGetRooms(currMessage);
					break;
				case 207:
					handleGetUsersInRoom(currMessage);
					break;
				case 209:
					handleJoinRoom(currMessage);
				case 211:
					handleLeaveRoom(currMessage);
					break;
				case 213:
					handleCreateRoom(currMessage);
					break;
				case 215:
					handleCloseRoom(currMessage);
					break;
				case 217:
					handleStartGame(currMessage);
					break;
				case 219:
					handlePlayerAnswer(currMessage);
					break;
				case 222:
					handleLeaveGame(currMessage);
					break;
				case 223:
					handleGetBestScores(currMessage);
					break;
				case 225:
					handleGetPersonalStatus(currMessage);
					break;
				default:
					safeDeleteUser(currMessage);
					break;
				}

				delete currMessage;
			}
			catch (...)
			{
				safeDeleteUser(currMessage);
			}
		}
		catch (...)
		{
		}
	}
}

RecievedMessage* MTServer::buildRecieveMessage(SOCKET client_socket, int msgCode)
{
	RecievedMessage* msg = nullptr;
	vector<string> values;
	if (msgCode == 200)
	{
		int userSize = Helper::getIntPartFromSocket(client_socket, 2);
		string userName = Helper::getStringPartFromSocket(client_socket, userSize);
		values.push_back(userName);
		int passSize = Helper::getIntPartFromSocket(client_socket, 2);
		string password = Helper::getStringPartFromSocket(client_socket, passSize);
		values.push_back(password);
	}
	else if (msgCode == 207)
	{
		string roomId = Helper::getStringPartFromSocket(client_socket, 4);
		values.push_back(roomId);

	}
	else if (msgCode == 203)
	{
		int userSize = Helper::getIntPartFromSocket(client_socket, 2);
		string userName = Helper::getStringPartFromSocket(client_socket, userSize);
		values.push_back(userName);
		int passSize = Helper::getIntPartFromSocket(client_socket, 2);
		string password = Helper::getStringPartFromSocket(client_socket, passSize);
		values.push_back(password);
		int emailSize = Helper::getIntPartFromSocket(client_socket, 2);
		string email = Helper::getStringPartFromSocket(client_socket, emailSize);
		values.push_back(email);

	}
	else if (msgCode == 207 || msgCode == 209)
	{
		string roomId = Helper::getStringPartFromSocket(client_socket, 4);
		values.push_back(roomId);
	}
	else if (msgCode == 213)
	{
		int nameSize = Helper::getIntPartFromSocket(client_socket, 2);
		string roomName = Helper::getStringPartFromSocket(client_socket, nameSize);
		values.push_back(roomName);
		string numPlayers = Helper::getStringPartFromSocket(client_socket, 1);
		values.push_back(numPlayers);
		string numQuestions = Helper::getStringPartFromSocket(client_socket, 2);
		values.push_back(numQuestions);
		string questionTime = Helper::getStringPartFromSocket(client_socket, 2);
		values.push_back(questionTime);
	}
	else if (msgCode == 219)
	{
		string numAns = Helper::getStringPartFromSocket(client_socket, 1);
		values.push_back(numAns);
		string time = Helper::getStringPartFromSocket(client_socket, 2);
		values.push_back(time);
	}
	msg = new RecievedMessage(client_socket, msgCode, values);
	return msg;

}

User* MTServer::handleSignin(RecievedMessage* msg)
{
	User* u = nullptr;
	vector<string> info = msg->getValues();
	if (!_db.isUserAndPassMatch(info[0], info[1]))
	{
		Helper::sendData(msg->getSock(), "1021");
	}
	else if (getUserByName(info[0]) != nullptr)
	{
		Helper::sendData(msg->getSock(), "1022");
	}
	else
	{
		u = new User(info[0], msg->getSock());
		std::pair<SOCKET, User*> p(msg->getSock(), u);
		_connectedUsers.insert(p);
		Helper::sendData(msg->getSock(), "1020");
	}
	return u;
}

bool MTServer::handleSignup(RecievedMessage* msg)
{
	bool ans = false;
	if (!Validator::isPassword(msg->getValues()[1]))
	{
		Helper::sendData(msg->getSock(), "1041");
	}
	else if (!Validator::isUsername(msg->getValues()[0]))
	{
		Helper::sendData(msg->getSock(), "1043");
	}
	else if (_db.isUserExists(msg->getValues()[0]))
	{
		Helper::sendData(msg->getSock(), "1042");
	}
	else
	{
		if (_db.addNewUser(msg->getValues()[0], msg->getValues()[1], msg->getValues()[2]))
		{
			Helper::sendData(msg->getSock(), "1040");
			ans = true;
		}
		else
		{
			Helper::sendData(msg->getSock(), "1044");
		}
	}
	return ans;
}

void MTServer::handleSignout(RecievedMessage* msg)
{
	User* u = getUserBySocket(msg->getSock());
	if (u != nullptr)
	{
		safeDeleteUser(msg);
		handleCloseRoom(msg);
		handleLeaveRoom(msg);
		handleLeaveGame(msg);
	}
}

void MTServer::handleLeaveGame(RecievedMessage* msg)
{
	if (getUserBySocket(msg->getSock())->leaveGame())
	{
		delete getUserBySocket(msg->getSock())->getGame();
	}
}

void MTServer::handleStartGame(RecievedMessage* msg)
{
	User* u = getUserBySocket(msg->getSock());
	try
	{
		Game* g = new Game((u->getRoom()->getUsers()), u->getRoom()->getQuestionsNum(), _db);
		u->setGame(g);
		_roomsList.erase(u->getRoom()->getId());
		g->sendFirstQuestion();
	}
	catch (...)
	{
		Helper::sendData(msg->getSock(), "1180");
	}
}

void MTServer::handlePlayerAnswer(RecievedMessage* msg)
{
	User* u = getUserBySocket(msg->getSock());
	Game* m = u->getGame();
	if (m != nullptr)
	{
		if (!m->handleAnswerFromUser(u, std::stoi(msg->getValues()[0]), std::stoi(msg->getValues()[1])))
		{
			delete m;
		}
	}
}

bool MTServer::handleCreateRoom(RecievedMessage* msg)
{
	bool ans = false;
	User* u = getUserBySocket(msg->getSock());
	if (u != nullptr)
	{
		vector<string> val = msg->getValues();
		_roomIdSequence++;
		if (u->createRoom(_roomIdSequence, msg->getValues()[0],
			std::stoi(msg->getValues()[1]), std::stoi(msg->getValues()[2]), std::stoi(msg->getValues()[3])))
		{
			std::pair<int, Room*> p(u->getRoom()->getId(), u->getRoom());
			_roomsList.insert(p);
			ans = true;
		}
	}
	return ans;
}

bool MTServer::handleCloseRoom(RecievedMessage* msg)
{
	bool ans = false;
	Room* r = getUserBySocket(msg->getSock())->getRoom();
	if (r != nullptr)
	{
		if (getUserBySocket(msg->getSock())->closeRoom() != -1)
		{
			_roomsList.erase(r->getId());
		}
	}
	return ans;
}

bool MTServer::handleJoinRoom(RecievedMessage* msg)
{
	bool ans = false;
	User* u = getUserBySocket(msg->getSock());
	if (u != nullptr)
	{

		int id = stoi(msg->getValues()[0]);
		Room* r = _roomsList.find(id)->second;
		if (r == nullptr)
		{
			Helper::sendData(msg->getSock(), "1102");
		}
		else
		{
			if (!u->joinRoom(r))
				Helper::sendData(msg->getSock(), "1101");
			else
			{
				string message = "1100" + Helper::getPaddedNumber(r->getQuestionsNum(), 2) + Helper::getPaddedNumber(r->getQuestionsTime(), 2);
				int i = 0;
				for (int i = 0; i < r->getUsers().size(); i++)
					Helper::sendData(r->getUsers()[i]->getSocket(), message);
				ans = true;
			}
		}
	}
	return ans;
}

bool MTServer::handleLeaveRoom(RecievedMessage* msg)
{
	bool ans = false;
	User* u = getUserBySocket(msg->getSock());
	if (u != nullptr)
	{
		Room* r = u->getRoom();
		if (!(r == nullptr) || !(to_string(r->getId()) != msg->getValues()[0]))
		{
			u->leaveRoom();
		}
	}
	return ans;
}

void MTServer::handleGetUsersInRoom(RecievedMessage* msg)
{
	User* u = getUserBySocket(msg->getSock());
	if (u != nullptr)
	{
		int id = stoi(msg->getValues()[0]);
		Room * r = _roomsList.find(id)->second;
		if (r == nullptr)
		{
			Helper::sendData(msg->getSock(), "1080");
		}
		else
		{
			string message = "108" + r->getUsersListMessage();
			Helper::sendData(msg->getSock(), message);
		}
	}
}

void MTServer::handleGetRooms(RecievedMessage* msg)
{
	string message = "106" + std::to_string(_roomsList.size());
	std::map<int, Room*>::iterator it2;
	for (it2 = _roomsList.begin(); it2 != _roomsList.end(); ++it2)
		message += (Helper::getPaddedNumber(it2->first, 4) + Helper::getPaddedNumber((it2->second)->getName().size(), 2) +
		(it2->second)->getName());
	Helper::sendData(msg->getSock(), message);
}


void MTServer::handleGetBestScores(RecievedMessage* msg)
{
	string message = "124";
	vector<string> v = _db.getBestScores();
	int i = 0;
	for (i = 0; i < v.size(); i += 2)
	{
		std::string username = v[i];
		if (username == "0")
			username = "";
		int score = std::stoi(v[i + 1]);
		int size = username.size();
		message += (Helper::getPaddedNumber(size, 2) + username + Helper::getPaddedNumber(score, 6));
	}
	Helper::sendData(msg->getSock(), message);
}

void MTServer::handleGetPersonalStatus(RecievedMessage* msg)
{
	User* u = getUserBySocket(msg->getSock());
	if (u != nullptr)
	{
		vector<string> v = _db.getPersonalStatus(u->getUserName());
		std::string ans = "126";
		ans = ans + Helper::getPaddedNumber(std::stoi(v[0]), 4);
		ans = ans + Helper::getPaddedNumber(std::stoi(v[1]), 6);
		ans = ans + Helper::getPaddedNumber(std::stoi(v[2]), 6);
		std::string num = v[3], first = "", second = "";
		char c = num[0];
		int i = 1;
		while (c != '.')
		{
			first += c;
			c = num[i];
			i++;
		}
		for (i = i; i < num.length(); i++)
			second += num[i];
		ans = ans + (Helper::getPaddedNumber(std::stoi(first), 2) + Helper::getPaddedNumber(std::stoi(second), 2));
		Helper::sendData(msg->getSock(), ans);
	}
}
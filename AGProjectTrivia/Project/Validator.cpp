
#include "Validator.h"
#include "Includes.h"

/*
This function check if the password is good and strong
input:password
output: false if not and true if it strong
*/
bool Validator::isPassword(std::string st)
{
	bool goodLen = false;
	bool isDigit = false, LowerCase = false;
	bool space = false, UpperCase = false;

	int len = st.size();
	if (len >= 4)
		goodLen = true;

	for (int i = 0; i < len; i++)
	{
		if (st[i] == ' ')
			space = true;
		if (isdigit(st[i]))
			isDigit = true;
		if (st[i] >= 'a' && st[i] <= 'z')
			LowerCase = true;
		if (st[i] >= 'A'&& st[i] <= 'Z')
			UpperCase = true;
	}

	if ((!space && isDigit) && (LowerCase&&UpperCase) && goodLen)
		return true;
	else
		return false;


}

/*
This function check if the user is strong and good
input:username
output:false if the username isnt good
true if the username is strong and good
*/
bool Validator::isUsername(std::string st)
{
	bool startAlpa = false, space = false;
	if(isalpha(st[0]))
		startAlpa = true;
	int i;
	for (i = 0; i < st.size(); i++)
	{
		if (st[i] == ' ')
			space = true;
	}

	if ((startAlpa && !space) && (st.length() != 0))
		return true;
	else
		return false;

	
}
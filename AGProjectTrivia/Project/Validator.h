#pragma once

#include "Includes.h"

static class Validator
{
public:
	static bool isPassword(std::string st);
	static bool isUsername(std::string st);
};
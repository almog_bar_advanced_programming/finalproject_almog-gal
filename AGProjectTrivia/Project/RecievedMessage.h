#pragma once

#include "Includes.h"
#include "User.h"

using namespace std;

class RecievedMessage
{
public:
	RecievedMessage(SOCKET sock, int messageCode);

	RecievedMessage(SOCKET sock, int messageCode, vector<string> values);

	SOCKET getSock();
	int getMessageCode();

	vector<string>& getValues();

	void setUser(User* u);
	User* getUser();

private:
	SOCKET _sock;
	int _messageCode;
	vector<string> _values;
	User* _user;
};


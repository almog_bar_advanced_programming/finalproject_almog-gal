#include "Game.h"
#include "DataBase.h"

/*
This function is the C'tor for the game
input: vector players,num of question and db
output:game
*/
Game::Game(const std::vector<User*>& players, int questionNum, DataBase& db):_players(players),_questionsNum(questionNum),_db(db)
{
	this->_db = *(new DataBase());
	try
	{
		int _id = _db.insertNewGame();
		this->_question = this->_db.initQuestions(questionNum);
		this->_players = players;
		for (int i = 0; i < _players.size(); i++)
		{
			this->_players[i]->setGame(this);
		}

	}
	catch(const std::exception& e)
	{
		std::cout << "Exception was catch in function insertNewGame "  << ", what=" << e.what() << std::endl;
	}
	this->_id = 0;
}

/*
This function is the D'tor
*/
Game::~Game()
{
	this->_question.clear();
	this->_players.clear();
}

/*
This function send the firstQuestion
input:none
output:none
*/
void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}

/*
This function make user leave the game
input:CurrUser
output:true if he is in the list and got delete or not
*/
bool Game::leaveGame(User* currUser)
{
	int i = 0;
	bool found = false;
	for (i = 0; i < this->_players.size(); i++)
	{
		if (this->_players[i]->getUserName() == currUser->getUserName())
		{
			found = true;
			break;
		}
	}
	if (found)
	{
		this->_players.erase(this->_players.begin() + i);
	}
	return found;
}

/*
This function insert the game into DB
input:none
ouput: false or true
*/
bool Game::insertGameToDB()
{
	int id = 0;
	try
	{
		id = this->_db.insertNewGame();
	}
	catch(...)
	{
		return false;
	}
	this->_id = id;
	return true;
}

/*
The function initQuestion from DB
input:none
output:none
*/
void Game::initQuestionFromDB()
{
	this->_db.initQuestions(this->_questionsNum);
}

/*
This function Send the question to all users in the game
input: none
output:none
*/
void Game::sendQuestionToAllUsers()
{
	int i = 0;
	std::string* ans;
	int ansFirst = 0, ansSeco = 0, ansTre = 0, ansFour = 0;
	int index = 0;
	this->_currentTurnAnswer = 0;
	for (i=0;i<this->_players.size();i++)
	{
		try
		{
			int lenQ = this->_question[this->_questionsNum]->getQuestion().size();
			std::string q = this->_question[_questionsNum]->getQuestion();
			ans = this->_question[this->_questionsNum]->getAnswer();
			ansFirst = ans[0].size();
			ansSeco = ans[1].size();
			ansTre = ans[2].size();
			ansFour = ans[3].size();

			this->_players[i]->send("118"+Helper::getPaddedNumber(lenQ,3)+q+ Helper::getPaddedNumber(ansFirst, 3)+ans[0]
									+Helper::getPaddedNumber(ansSeco,3)+ans[1]+Helper::getPaddedNumber(ansTre,3)+ans[2]
									+Helper::getPaddedNumber(ansFour,3)+ans[3]);
		}
		catch (const std::exception& e)
		{
			std::cout << "Exception was catch in function SendQuestionToAllUsers in Game " << ", what=" << e.what() << std::endl;
		}
	}
}

int Game::getId()
{
	return this->_id;
}

/*
This function Handle the answer from user
input:user,answernumIndex,timeTook
output:false or true
*/
bool Game::handleAnswerFromUser(User* user, int answerNumberIndex, int timeTook)
{
	this->_currentTurnAnswer++;
	if (answerNumberIndex == this->_question[_questionsNum]->getCorrentAnswerIndex())
	{
		this->_result.insert(std::pair<std::string, int>(user->getUserName(), 1));
	}

	return (this->_currentTurnAnswer == this->_currQuestionIndex);
}

/*
This function Handle how to finish the game
input:none
output: none
*/
void Game::handleFinishGame()
{
	int i = 0;
	for (i = 0; i<this->_players.size(); i++)
	{
		try
		{
			this->_players[i]->send("121" + std::to_string(this->_players.size()) + Helper::getPaddedNumber(this->_players[i]->getUserName().size(), 2)
				+ this->_players[i]->getUserName() + Helper::getPaddedNumber(this->_players[i]->getGame()->_result.size(), 2)
				+ std::to_string(this->_players[i]->getGame()->_result.size()));
		}
		catch (const std::exception& e)
		{
			std::cout << "Exception was catch in function handleFinishGame" << ", what=" << e.what() << std::endl;
		}
	}
}

/*
This function handlethe next turn in the game 
input:none
output:true or false
*/
bool Game::handleNextTurn()
{

	if (this->_players.empty())
	{
		handleFinishGame();
		return false;
	}
	else
	{
		if (this->_currentTurnAnswer == this->_players.size())
		{
			if (this->_currQuestionIndex == this->_questionsNum)
				handleFinishGame();
			else
			{
				this->_questionsNum++;
				sendQuestionToAllUsers();
				return true;
			}
		}
	}

}

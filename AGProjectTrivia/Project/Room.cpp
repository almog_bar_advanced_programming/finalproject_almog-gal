#include "Room.h"

/*
This function is the C'tor of the class room
input:id, admin,name,maxUsers,questionNum and questionTime
output: Room
*/
Room::Room(int id, User* admin, std::string name, int maxUsers, int questionNum, int questionTime)
{
	this->_roomId = id;
	this->_admin = admin;
	this->_roomname = name;
	this->_maxUser = maxUsers;
	this->_questionsNum = questionNum;
	this->_questionsTime = questionTime;
	std::vector<User*> userList;
	userList.push_back(admin);
	this->_users = userList;

}

/*
This function get the users in the vector in to string
input: userslist, excludeUser
output: the list of users as a string
*/
std::string Room::getUsersAsString(std::vector<User*> usersList, User* excludeUser)
{
	std::string listUsers;
	for (int i = 0; i < usersList.size(); i++)
	{
		if (usersList[i] != excludeUser)
		{
			listUsers += (usersList[i]->getUserName() + "\n");
		}
	}
	return listUsers;
}

/*
This function return a string of a message
input:none
output: message

*/
std::string Room::getUsersListMessage()
{
	std::string msg;
	std::string user;
	std::string code = "108";
	int num = this->_users.size();
	std::string players = "" + std::to_string(num);
	int lenUser = 0;
	msg = code + players;
	for (int i = 0; i < this->_users.size(); i++)
	{
		user = this->_users[i]->getUserName();
		lenUser = user.length();
		msg += Helper::getPaddedNumber(lenUser, 2) + user;
	}
	return msg;
}

/*
This function send all the users except the excludeUser a msg
input: excldeUser and  the msg
output:void
*/
void Room::sendMessage(User* excludeUser, std::string msg)
{
	User* user = nullptr;
	for (int i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i] != excludeUser)
		{
			this->_users[i]->send(msg);
		}
	}
}


void Room::sendMessage(std::string msg)
{
	sendMessage(nullptr, msg);
}

/*
This function check if the user can join the room or not
input: user
output:true/false
*/
bool Room::joinRoom(User* user)
{
	bool flag = false;
	if (this == nullptr)
		user->send("1102");	// failed
	if (this->_users.size() < this->_maxUser)
	{
		this->_users.push_back(user);//success
		sendMessage("1100" + std::to_string(this->_questionsNum) + " " + std::to_string(this->_questionsTime));
		getUsersListMessage();
		flag = true;
	}
	else
	{
		flag = false;
		user->send("1101"); //failed
	}

	return flag;
}
/*
This function check if the user that want to leave is in the room
and if he is, the function erase him and send a msg to the other players and to the user leaving
input: the leaving user
output:none
*/
void Room::leaveRoom(User* user)
{
	std::vector<User*> usersList;
	User* eraseUser;
	for (int i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i]->getUserName() != user->getUserName())
		{
			usersList.push_back(this->_users[i]);
		}
		else
		{
			user->send("1120");//failed
		}
	}
	this->_users = usersList;
	sendMessage(getUsersListMessage());
}


/*
This function close the room
input: the user
output: the id of the room but if the room can't be close the
function will return -1
*/
int	Room::closeRoom(User* user)
{
	User* userN;
	if (user == this->_admin)
	{
		sendMessage("116");// ����� ������ ����.

		for (int i = 0; i < this->_users.size(); i++)
		{
			if (this->_users[i] != _admin)
			{
				userN = this->_users[i];
				userN->clearRoom();
			}
		}

	}
	else
		return -1;
	return this->_roomId;
}

std::vector<User*> Room::getUser()
{
	return this->_users;
}

int Room::getQuestionsNum()
{
	return this->_questionsNum;
}

int Room::getQuestionsTime()
{
	return this->_questionsTime;
}


int Room::getId()
{
	return this->_roomId;
}


std::string	Room::getName()
{
	return this->_roomname;
}

std::vector<User*> Room::getUsers()
{
	return this->_users;
}
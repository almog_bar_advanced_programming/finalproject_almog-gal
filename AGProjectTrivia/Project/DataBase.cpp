#include "DataBase.h"
#include <unordered_map>

std::vector<std::string> results;
std::vector<std::string> users;
std::vector<Question*> questions;
static sqlite3* _db;
static int gameId = 674;

int DataBase::callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		results.push_back(argv[i]);
	}
	return 0;
}

DataBase::DataBase()
{
	sqlite3* db;
	int rc = sqlite3_open("trivia.db", &db);

	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		sqlite3_close(db);
		throw std::exception("unable to open database");
	}

	_db = db;
}

DataBase::~DataBase()
{
	sqlite3_close(_db);
}

bool DataBase::isUserExists(std::string username)
{
	results.clear();
	char *zErrMsg = 0;
	std::string str = "select password from t_users where name=\"" + username + "\"";
	int rc = sqlite3_exec(_db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		return false;
	}
	if (results.empty() == true)
	{
		return false;
	}
	return true;
}

bool DataBase::addNewUser(std::string userName, std::string passowrd, std::string email)
{
	char *zErrMsg = 0;
	std::string st = "insert into t_users(username, password, email) values(\"" + userName + "\", \"" + passowrd + "\", \"" + email + "\")";
	int rc = sqlite3_exec(_db, st.c_str(), NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		throw std::exception("sql error");
		return false;
	}
	return true;
}

bool DataBase::isUserAndPassMatch(std::string userName, std::string password)
{
	results.clear();
	char *zErrMsg = 0;
	std::string str = "select password from t_users where username=\"" + userName + "\"";
	int rc = sqlite3_exec(_db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		throw std::exception("sql error");
		return false;
	}
	if (results.empty() == true)
	{
		return false;
	}
	if (results[0] == password)
	{
		return true;
	}
	return false;
}

std::vector<Question*> DataBase::initQuestions(int questionNum)
{
	srand(time(NULL));
	std::vector<Question*> reqQuestions;
	char *zErrMsg = 0;
	std::string str = "select * from t_questions";
	int rc = sqlite3_exec(_db, str.c_str(), callBackQuestions, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		throw std::exception("sql error");
	}
	int i = 0, x = 0;
	for (i = 0; i < questionNum; i++)
	{
		x = rand() % questions.size();

		reqQuestions.push_back(questions[x]);
	}
	return reqQuestions;
}

std::vector<std::string> DataBase::getBestScores()
{
	std::vector<std::string> ans;
	std::vector<std::string> scores;
	users.clear();
	char *zErrMsg = 0;
	std::string str = "select username from t_users";
	int rc = sqlite3_exec(_db, str.c_str(), callBackUsers, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		throw std::exception("sql error");
	}
	int i = 0, j = 0, numCorrect = 0;
	for (i = 0; i < users.size(); i++)
	{
		numCorrect = 0;
		results.clear();
		std::string str = "select is_correct from t_players_answers where username=\"" + users[i] + "\"";
		int rc = sqlite3_exec(_db, str.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			std::cout << "SQL error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			throw std::exception("sql error");
		}
		for (j = 0; j < results.size(); j++)
		{
			if (results[j] == "1")
				numCorrect++;
		}
		scores.push_back(std::to_string(numCorrect));
	}

	int first, second, third, index1 = -1, index2 = -1, index3 = -1;

	third = first = second = -1;
	for (i = 0; i < scores.size(); i++)
	{
		if (std::stoi(scores[i]) > first)
		{
			third = second;
			index3 = index2;
			second = first;
			index2 = index1;
			first = std::stoi(scores[i]);
			index1 = i;
		}

		else if (std::stoi(scores[i]) > second)
		{
			third = second;
			index3 = index2;
			second = std::stoi(scores[i]);
			index1 = i;
		}

		else if (std::stoi(scores[i]) > third)
		{
			third = std::stoi(scores[i]);
			index3 = i;
		}
	}
	if (index1 != -1)
	{
		ans.push_back(users[index1]);
		ans.push_back(scores[index1]);
	}
	else
	{
		ans.push_back("0");
		ans.push_back("0");
	}
	if (index2 != -1)
	{
		ans.push_back(users[index2]);
		ans.push_back(scores[index2]);
	}

	else
	{
		ans.push_back("0");
		ans.push_back("0");
	}
	if (index3 != -1)
	{
		ans.push_back(users[index3]);
		ans.push_back(scores[index3]);
	}
	else
	{
		ans.push_back("0");
		ans.push_back("0");
	}
	return ans;
}

std::vector<std::string> DataBase::getPersonalStatus(std::string username)
{
	std::vector<std::string> ans;
	results.clear();
	char *zErrMsg = 0;
	std::string str = "select is_correct from t_players_answers where username=\"" + username + "\"";
	int rc = sqlite3_exec(_db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		throw std::exception("sql error");
	}
	int i = 0, numCorrect = 0, numWrong = 0;
	for (i = 0; i < results.size(); i++)
	{
		if (results[i] == "1")
			numCorrect++;
	}
	numWrong = (results.size() - numCorrect);

	results.clear();
	str = "select game_id from t_players_answers where username=\"" + username + "\"";
	rc = sqlite3_exec(_db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		throw std::exception("sql error");
	}
	std::vector<std::string> temp;
	int numGames = 0, j = 0;
	for (i = 0; i < results.size(); i++)
	{
		bool flag = false;
		for (j = 0; j < temp.size(); j++)
		{
			if (results[i] == temp[j])
				flag = true;
		}
		if (!flag)
		{
			temp.push_back(results[i]);
			numGames++;
		}
	}

	results.clear();
	str = "select answer_time from t_players_answers where username=\"" + username + "\"";
	rc = sqlite3_exec(_db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		throw std::exception("sql error");
	}
	int sum = 0;
	for (i = 0; i < results.size(); i++)
	{
		sum += std::stoi(results[i]);
	}
	double avgTime = (double)sum / results.size();

	ans.push_back(std::to_string(numGames));
	ans.push_back(std::to_string(numCorrect));
	ans.push_back(std::to_string(numWrong));
	ans.push_back(std::to_string(avgTime));

	return ans;
}

int DataBase::insertNewGame()
{
	char timeStr[9];
	_strtime_s(timeStr);

	gameId++;

	char *zErrMsg = 0;
	std::string str = "insert into t_games(game_id, status, start_time) values(" + std::to_string(gameId) + ",0,\"" + timeStr + "\")";

	int rc = sqlite3_exec(_db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		throw std::exception("sql error");
	}
	return (gameId - 1);
}

bool DataBase::updateGameStatus(int idGame)
{
	char timeStr[9];
	_strtime_s(timeStr);
	std::string str(timeStr);

	char *zErrMsg = 0;
	str = "update t_games set status=1 where game_id=" + std::to_string(idGame);
	int rc = sqlite3_exec(_db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		throw std::exception("sql error");
		return false;
	}
	str = "update t_games set time=\"" + str + "\" where game_id=" + std::to_string(idGame);
	rc = sqlite3_exec(_db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		throw std::exception("sql error");
		return false;
	}
	return true;
}

bool DataBase::addAnswerToPlayer(int gameId, std::string userName, int questionId, std::string answer, bool isCorrect, int answerTime)
{
	char timeStr[9];
	_strtime_s(timeStr);

	if (isCorrect)
		std::string str = "1";
	else
		std::string str = "0";

	char *zErrMsg = 0;
	std::string str = "insert into t_players_answers(game_id, username, question_id, player_answer, is_correct, answer_time) values(" + std::to_string(gameId) + ",\"" + userName + "\","
		+ std::to_string(questionId) + ",\"" + answer + "\"," + str + "," + std::to_string(answerTime) + ")";

	int rc = sqlite3_exec(_db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		throw std::exception("sql error");
		return false;
	}
	return true;
}

int DataBase::callBackUsers(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		users.push_back(argv[i]);
	}
	return 0;
}
int DataBase::callBackQuestions(void* notUsed, int argc, char** argv, char** azCol)
{
	int i, j;
	std::string id, ques, ans1, ans2, ans3, ans4;
	id = argv[0];
	ques = argv[1];
	ans1 = argv[2];
	ans2 = argv[3];
	ans3 = argv[4];
	ans4 = argv[5];
	Question* q = new Question(atoi(id.c_str()), ques, ans1, ans2, ans3, ans4);
	questions.push_back(q);
	return 0;
}

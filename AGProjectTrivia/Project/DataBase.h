#pragma once
#include "Includes.h"
#include "Question.h"
#include "sqlite3.h"
#include <ctime>

class DataBase
{
private:
	static int callBackUsers(void* notUsed, int argc, char** argv, char** azCol);
	static int callBackQuestions(void* notUsed, int argc, char** argv, char** azCol);
	static int callback(void* notUsed, int argc, char** argv, char** azCol);

public:
	DataBase();
	~DataBase();
	bool isUserExists(std::string username);
	bool addNewUser(std::string userName, std::string passowrd, std::string email);
	bool isUserAndPassMatch(std::string userName, std::string password);
	std::vector<Question*> initQuestions(int questionNum);
	std::vector<std::string>getBestScores();
	std::vector<std::string>getPersonalStatus(std::string username);
	int insertNewGame();
	bool updateGameStatus(int idGame);
	bool addAnswerToPlayer(int gameId, std::string userName, int questionId, std::string answer, bool isCorrect, int answerTime);
};
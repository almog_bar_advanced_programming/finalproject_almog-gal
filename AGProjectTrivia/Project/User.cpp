#include "User.h"

/*This function is the C'tor of the User
input: user name and socket
*/
User::User(std::string userName, SOCKET sock)
{
	this->_userName = userName;
	this->_socket = sock;
	this->_currGame = nullptr;
	this->_currRoom = 0;
}

/*
This function send the msg to the user
input: the msg
output: none
*/
void User::send(std::string msg)
{
	Helper::sendData(this->_socket, msg);
}


/*
This function return the user name
input: none
output:user name
*/
std::string User::getUserName()
{
	return this->_userName;
}

/*
This function return the room
input:none
output:room
*/
Room* User::getRoom()
{
	return this->_currRoom;
}

/*
This function return the game
input:none
output: game
*/
Game* User::getGame()
{
	return this->_currGame;
}

/*
This function set the current game into a new game
input: Game* game
output:none
*/
void User::setGame(Game* game)
{
	this->_currGame = game;
}

/*
This function clear the room
input:none
output:none
*/
void User::clearRoom()
{
	this->_currGame = nullptr;
}

/*
This function check if the user is connected to the room
if not the function will create a new room and connect to the user
and will send a success msg
if it is the function will send a fail msg
input: roomId, roomName, maxUsers, questionNum and questionTime
output: bool
*/
bool User::createRoom(int roomId, std::string roomName, int maxUsers, int questionNum, int questionTime)
{
	bool flag = false;
	std::string success = "1140";
	std::string fail = "1141";
	Room* a = new Room(roomId, this, this->_userName, maxUsers, questionNum, questionTime);
	if (this->_currRoom == nullptr)
	{
		flag = true;
		_currRoom = a;
		Helper::sendData(this->_socket, success);
	}
	else
	{
		Helper::sendData(this->_socket, fail);
	}
	return flag;
}

bool User::joinRoom(Room* newRoom)
{
	User* newUser = new User(_userName, _socket);
	bool flag = _currRoom->joinRoom(this);///////////////TACALA
	return flag;
}

void User::leaveRoom()
{
	if (this->_currRoom != nullptr)
	{
		_currRoom->leaveRoom(this);
	}
}

/*
This function close the room
input:none
output: roomID or -1 if the room hasnt closed
*/
int User::closeRoom()
{
	int roomId;
	if (this->_currRoom == nullptr)//not in the room
	{
		return -1;
	}
	else //in the room 
	{
		roomId = this->_currRoom->closeRoom(this);///PROBLEM!
		if (roomId != -1)
		{
			this->_currRoom = nullptr;
		}
	}
	return roomId;

}
/*
This function let the user to leave the game
input:none
output:true or false
*/
bool User::leaveGame()
{
	bool flag;                            ///check!!!!!
	flag = this->_currGame->leaveGame(this);
	if (flag == false)
	{
		this->_currGame = nullptr;
	}
	return flag;
}

SOCKET User::getSocket()
{
	return _socket;
}
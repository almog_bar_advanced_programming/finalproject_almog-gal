#pragma once

#include "Includes.h"
#include "User.h"

class User;

class Room
{
private:
	std::vector <User*> _users;
	User* _admin;
	int _maxUser;
	int _questionsTime;
	int _questionsNum;
	std::string _roomname;
	int _roomId;


	std::string getUsersAsString(std::vector<User*> usersList, User* excludeUser);
	void sendMessage(User* excludeUser, std::string msg);
	void sendMessage(std::string msg);

public:

	Room(int id, User* admin, std::string name, int maxUsers, int questionNum, int questionTime);
	std::string getUsersListMessage();

	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int	closeRoom(User* user);
	std::vector<User*> getUser();
	int getQuestionsNum();
	int getQuestionsTime();
	int getId();
	std::string	getName();
	std::vector<User*> getUsers();



	
};


